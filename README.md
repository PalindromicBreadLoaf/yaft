# yaft

yaft - Yet Another Fetch Tool

An incredibly simple fetch tool that fetches absoulely no useful information about your device, and instead chastises you for trying to use it. Only runs on Unix-based systems as the Windows port is busted.

How to use:

Clone the repo (Or alternatively just download the yaft.sh/yaft.bat file directly.)
```
git clone https://gitlab.com/PalindromicBreadLoaf/yaft
```
Open a shell wherever the yaft.sh/yaft.bat file is located.
Give the file executable privileges (Unix only)
```
chmod +x yaft.sh
```
Run the file
```
./yaft.sh
```
Or for Windows
```
./yaft.bat
```

Enjoy. 

Credits and Attributions:

The vulgar penguin ASCII art was not made by me, but I do not know how made it, but credit is due to whomever it was.
A dumb post I saw elsewhere on the internet inspired me to make this. Has this been done before? Probably. Do I much care? No.
